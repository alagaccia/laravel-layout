/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap');

const flatpickr = require("flatpickr").default;
const italian = require("flatpickr/dist/l10n/it.js").default.it;
flatpickr.localize(italian);

window.accounting = require('accounting');
window.autocomplete = require('devbridge-autocomplete');
// window.GeminiScrollbar = require('gemini-scrollbar');
window.moment = require('moment');
window.selection = require('@simonwep/selection-js');
window.BulmaTagsInput = require('@creativebulma/bulma-tagsinput').default;
window.BulmaSwitch = require('bulma-switch');

require('./components/accordion');
require('./components/ajax');
require('./components/autocomplete');
require('./components/card');
require('./components/checkbox');
require('./components/datetimepicker');
require('./components/destroy');
require('./components/dropdown');
require('./components/header');
require('./components/modal');
require('./components/navbar');
require('./components/notification');
require('./components/notify');
require('./components/session');
// require('./components/sidebar');
require('./components/sms');
require('./components/submit');
require('./components/tables');
require('./components/tabs');
require('./components/tag-input');
require('./components/upload');
require('./components/viewer');
